# Centos-7 adapted spec.
# Original spec: https://github.com/blackav/ejudge/blob/master/build/fedora/ejudge.spec

%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}
%global _www_dir %{_var}/www
%global _htdocs_dir %{_var}/www/html
%global _cgi_bin_dir %{_var}/www/cgi-bin
%global _ej_group ejudge
%global _ej_user ejudge

%define _ejudge_version 3.7.8

Name: ejudge
Version: %{_ejudge_version}
Release: 1%{?dist}
Summary: A programming contest management system
Source: https://github.com/blackav/ejudge/archive/v%{version}.tar.gz
License: GPL
URL: http://ejudge.ru


BuildRequires: make 
# BuildRequires: gcc TODO: deltoolset
BuildRequires: glibc-devel 
BuildRequires: glibc-static 
BuildRequires: bison 
BuildRequires: flex 
BuildRequires: gawk 
BuildRequires: sed 
BuildRequires: zlib 
BuildRequires: zlib-devel 
BuildRequires: ncurses 
BuildRequires: ncurses-devel 
BuildRequires: expat 
BuildRequires: expat-devel 
BuildRequires: libzip 
BuildRequires: libzip-devel 
BuildRequires: gettext 
BuildRequires: gettext-devel 
BuildRequires: mysql-libs 
BuildRequires: mysql 
BuildRequires: mysql-devel 
BuildRequires: libcurl 
BuildRequires: libcurl-devel 
BuildRequires: libuuid 
BuildRequires: libuuid-devel 
BuildRequires: elfutils-libelf-devel 
BuildRequires: elfutils-libelf-devel-static 
BuildRequires: elfutils-libelf 
BuildRequires: libdwarf-devel 
BuildRequires: libdwarf-static 
BuildRequires: libdwarf 
BuildRequires: libdwarf-tools
BuildRequires: systemd-units
# BuildRequires: gcc-c++

Requires: make 
# Requires: gcc 
Requires: glibc-devel 
Requires: glibc-static 
Requires: bison 
Requires: flex 
Requires: gawk 
Requires: sed 
Requires: zlib 
Requires: zlib-devel 
Requires: ncurses 
Requires: ncurses-devel 
Requires: expat 
Requires: expat-devel 
Requires: libzip 
Requires: libzip-devel 
Requires: gettext 
Requires: gettext-devel 
Requires: mysql-libs 
Requires: mysql 
Requires: mysql-devel 
Requires: libcurl 
Requires: libcurl-devel 
Requires: libuuid 
Requires: libuuid-devel 
Requires: elfutils-libelf-devel 
Requires: elfutils-libelf-devel-static 
Requires: elfutils-libelf 
Requires: libdwarf-devel 
Requires: libdwarf-static 
Requires: libdwarf 
Requires: libdwarf-tools 
# Requires: gcc-c++ 
Requires: libstdc++-static 
Requires: fpc 
Requires: ruby 
Requires: python 
Requires: python3 
Requires: php 
Requires: php-common 
Requires: php-cli 
Requires: perl 
Requires: gprolog 
Requires: ghc 
Requires: mono-core 
Requires: mono-basic 
Requires: gcc-gfortran 
Requires: libgfortran-static 
Requires: gcc-go 
Requires: libgo-static 
Requires: mono-extras
Requires: mono-locale-extras 
Requires: valgrind 
Requires: nasm 
Requires: vim 
Requires: screen 
Requires: wget 
Requires: ncftp 
Requires: mc 
Requires: fuse-sshfs 
Requires: patch 
Requires: kernel-tools 
Requires: kernel-devel 
# Requires: gcc 
Requires: strace 
Requires: subversion 
Requires: gdb 
Requires: openssl 
Requires: openssl-devel 
Requires: java-1.8.0-openjdk 
Requires: java-1.8.0-openjdk-headless 
Requires: java-1.8.0-openjdk-devel

Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
Requires(pre): shadow-utils

%description
A programming contest management system. http://ejudge.ru

%prep
%autosetup -n %{name}-%{version}

%build
%configure \
  --enable-charset=utf-8 \
  --with-httpd-cgi-bin-dir=%{_cgi_bin_dir} \
  --with-httpd-htdocs-dir=%{_htdocs_dir} \
  --enable-ajax \
  --enable-local-dir=%{_sharedstatedir}/ejudge \
  --enable-hidden-server-bins \
  --disable-rpath

export EJUDGE_NO_CHECK_PATH=1
make # %{?_smp_mflags}

%install
%make_install

install --directory --mode 755 %{buildroot}%{_unitdir}
install --directory --mode 755 %{buildroot}%{_sysconfdir}/sysconfig

%{buildroot}%{_bindir}/ejudge-upgrade-web \
  --copy \
  --sandbox \
  --destdir %{buildroot}/

install --mode 644 \
  %{_builddir}/%{name}-%{version}/build/fedora/%{_arch}/ejudge-install.sh \
  %{buildroot}%{_bindir}/

install --mode 644 \
  %{_builddir}/%{name}-%{version}/boot/systemd/ejudge.service \
  %{buildroot}%{_unitdir}/

install --mode 644 \
   %{_builddir}/%{name}-%{version}/boot/systemd/ejudge \
   %{buildroot}%{_sysconfdir}/sysconfig/

export DONT_STRIP=1

%clean
rm --recursive --force %{buildroot}

%files
%{_bindir}/*
%{_libdir}/*
%{_datadir}/%{name}/
%{_libexecdir}/%{name}/
%{_includedir}/%{name}/
%{_datadir}/locale/
%{_unitdir}/*
%{_sysconfdir}/sysconfig/*
%{_htdocs_dir}/*
%{_cgi_bin_dir}/*
%{_includedir}/libdwarf-internal/*
%{_includedir}/libdwarf-internal/.gitignore
%{_prefix}/lib/ejudge/*

%pre
getent group %{_ej_group} >/dev/null || groupadd %{_ej_group}
getent passwd %{_ej_user} >/dev/null || useradd -g %{_ej_group} -d /home/%{_ej_user} -s /bin/bash -c "Ejudge programming contest management system" %{_ej_user}
exit 0

%post
%{_sbindir}/ldconfig

%postun
%{_sbindir}/ldconfig