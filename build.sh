#!/usr/bin/env bash

set -e
set -x

readonly tag="ej_builder"
readonly rpm_result_dir="/root/rpmbuild/RPMS/x86_64/"

docker rmi --force "${tag}"

docker build \
    --tag "${tag}" .

docker run \
    --rm \
    --volume "$(pwd):${rpm_result_dir}" \
    --entrypoint '/bin/bash' \
    "${tag}" \
    -c 'rpmbuild -bb /root/rpmbuild/SPECS/ejudge.spec'

docker rmi --force "${tag}"