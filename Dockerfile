from centos:latest

env PATH=/opt/rh/devtoolset-7/root/usr/bin:${PATH}
env EJ_VERSION=3.7.8

# Install repos and necessary requirements
run yum install --assumeyes epel-release centos-release-scl && \
    yum clean all && \
    yum install --assumeyes devtoolset-7-gcc* rpm-build rpm-devel \
        rpmlint make python bash coreutils diffutils \
        patch rpmdevtools which && \
    rpmdev-setuptree

# Copy the spec into the image
copy ./ejudge.spec /root/rpmbuild/SPECS/ejudge.spec

# Get rid of Centos repos (let builddep make its job)
run rm --force \
        /etc/yum.repos.d/CentOS-Vault.repo \
        /etc/yum.repos.d/CentOS-SCL*

# Install requires
run yum-builddep --assumeyes /root/rpmbuild/SPECS/ejudge.spec

# Get source tarball
run spectool --get-files --sourcedir /root/rpmbuild/SPECS/ejudge.spec
